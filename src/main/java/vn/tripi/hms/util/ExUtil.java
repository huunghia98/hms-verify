package vn.tripi.hms.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExUtil {
    public static String getThrowableDescription(Throwable throwable) {
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter(sw));
        return throwable.getMessage() + "------------" + sw.toString();
    }
}
