package vn.tripi.hms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = "vn.tripi")
@PropertySources({
        @PropertySource("classpath:application.yml"),
})
@EnableCaching
public class HMSVerifyApplication {
    public static void main(String[] args) {
        SpringApplication.run(HMSVerifyApplication.class, args);
    }
}
