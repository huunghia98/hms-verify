package vn.tripi.hms.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * @author Toan Pham on 04/09/2020
 */
@RequiredArgsConstructor
@Configuration
@EnableJpaRepositories(
        basePackages = "vn.tripi.hms",
        entityManagerFactoryRef = "hmsEntityManager",
        transactionManagerRef = "hmsTransactionManager")
@EnableTransactionManagement
public class HMSDatabaseConfig extends BaseDatasourceConfig {

    private final Environment env;

    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean hmsEntityManager() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(hmsDataSource());
        em.setPackagesToScan("vn.tripi.hms.dao");
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaPropertyMap(getCommonConfig(env));
        return em;
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasources.hms")
    public HikariConfig getHmsConfig(){
        return new HikariConfig();
    }

    @Primary
    @Bean
    public DataSource hmsDataSource() {
        return new HikariDataSource(getHmsConfig());
    }

    @Primary
    @Bean
    public PlatformTransactionManager hmsTransactionManager() {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(hmsEntityManager().getObject());
        return transactionManager;
    }
}
