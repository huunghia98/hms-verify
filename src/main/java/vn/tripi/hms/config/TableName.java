package vn.tripi.hms.config;

public interface TableName {
//    String AVAIL = "20210429_prd_room_availability";
//    String AVAIL_MONTHLY = "20210429_prd_room_availability_monthly";
//    String RATE = "20210429_prd_room_rate";
//    String RATE_MONTHLY = "20210429_prd_room_rate_monthly";

    String AVAIL = "room_availability";
    String AVAIL_MONTHLY = "room_availability_monthly";
    String RATE = "room_rate";
    String RATE_MONTHLY = "room_rate_monthly";

//    String AVAIL = "20210427_room_availability";
//    String AVAIL_MONTHLY = "20210427_room_availability_monthly";
//    String RATE = "20210427_room_rate";
//    String RATE_MONTHLY = "20210427_room_rate_monthly";
}
