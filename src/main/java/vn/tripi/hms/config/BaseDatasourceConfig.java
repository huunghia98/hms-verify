package vn.tripi.hms.config;

import org.springframework.core.env.Environment;

import java.util.HashMap;

/**
 * @author Toan Pham on 04/09/2020
 */
public class BaseDatasourceConfig {
    protected HashMap<String, Object> getCommonConfig(Environment env){
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
        properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.put("hibernate.show-sql", env.getProperty("spring.jpa.show-sql"));
        properties.put("hibernate.format_sql", env.getProperty("spring.jpa.properties.hibernate.format_sql"));
        properties.put("hibernate.enable_lazy_load_no_trans", env.getProperty("spring.jpa.properties.hibernate.enable_lazy_load_no_trans"));
        properties.put("hibernate.query.plan_cache_max_size", env.getProperty("spring.jpa.properties.hibernate.query.plan_cache_max_size"));
        properties.put("hibernate.query.in_clause_parameter_padding", env.getProperty("spring.jpa.properties.hibernate.query.in_clause_parameter_padding"));
        properties.put("hibernate.jdbc.batch_size", env.getProperty("spring.jpa.properties.hibernate.jdbc.batch_size"));
        properties.put("hibernate.order_inserts", env.getProperty("spring.jpa.properties.hibernate.order_inserts"));
        properties.put("hibernate.generate_statistics", env.getProperty("spring.jpa.properties.hibernate.generate_statistics"));
        properties.put("hibernate.connection.release_mode", "after_transaction");
        return properties;
    }
}
