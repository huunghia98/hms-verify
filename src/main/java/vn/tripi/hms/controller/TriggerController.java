package vn.tripi.hms.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import vn.tripi.core.common.vo.LangResponseBuilder;
import vn.tripi.core.common.vo.Response;
import vn.tripi.hms.service.AlmVerify;
import vn.tripi.hms.service.RateVerify;

import java.util.List;

@RestController
@AllArgsConstructor
public class TriggerController {
    private AlmVerify almVerify;
    private RateVerify rateVerify;

    @PostMapping("/alm")
    public Response triggerVerifyAllotment(@RequestBody(required = false) List<Long> ids) {
        return LangResponseBuilder.success(almVerify.getReport(ids));
    }

    @PostMapping("/rate")
    public Response triggerVerifyRate(@RequestBody(required = false) List<Long> ids) {
        return LangResponseBuilder.success(rateVerify.getReport(ids));
    }
}
