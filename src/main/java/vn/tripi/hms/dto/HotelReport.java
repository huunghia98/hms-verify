package vn.tripi.hms.dto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
@Accessors(chain = true)
public class HotelReport {
    private Long rootHotelId;
    private Boolean isDataError = false;
    private List<String> missingData = new ArrayList<>();
    private String description;
}
