package vn.tripi.hms.dto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import vn.tripi.hms.repo.HotelRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
@Accessors(chain = true)
public class Report {
    private List<Long> exceptions;
    // duplicate data
    private List<Long> errorData;
    // wrong data
    private List<Long> missingData;
    private List<HotelReport> reports;
}
