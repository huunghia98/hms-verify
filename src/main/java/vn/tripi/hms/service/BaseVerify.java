package vn.tripi.hms.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import vn.tripi.hms.dto.HotelReport;
import vn.tripi.hms.dto.Report;
import vn.tripi.hms.repo.HotelRepo;
import vn.tripi.hms.util.ExUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public abstract class BaseVerify {
    public static final LocalDate MAX_DATE = LocalDate.of(2031,1,1);
    public static final LocalDate MIN_DATE = LocalDate.of(2021,5,1);
    @Qualifier("executorServiceDefault")
    public final ExecutorService executorService;
    public final HotelRepo hotelRepo;

    public Report getReport(List<Long> rootHotelIds) {
        if (rootHotelIds == null) {
            rootHotelIds = hotelRepo.getAllRootIds();
        }
        List<CompletableFuture<HotelReport>> futures = new ArrayList<>();
        for (var rootHotelId : rootHotelIds) {
            CompletableFuture<HotelReport> future = CompletableFuture.supplyAsync(new Supplier<HotelReport>() {
                @Override
                public HotelReport get() {
                    HotelReport hotelReport = new HotelReport();
                    hotelReport.setRootHotelId(rootHotelId);
                    try {
                        hotelReport = getSelf().verifyHotel(rootHotelId);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        hotelReport.setDescription(ExUtil.getThrowableDescription(ex));
                    }
                    return hotelReport;
                }
            }, executorService);
            futures.add(future);
        }
        var waitAll = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
        waitAll.join();
        List<HotelReport> reports = futures.stream().map(CompletableFuture::join).collect(Collectors.toList());
        Report report = new Report();
        List<HotelReport> keepReports = new ArrayList<>();
        List<Long> exceptions = new ArrayList<>();
        List<Long> errorData = new ArrayList<>();
        List<Long> missingData = new ArrayList<>();

        for (var rp : reports) {
            if (rp.getDescription() != null || rp.getIsDataError() || rp.getMissingData().size() > 0) {
                if (rp.getDescription() != null) exceptions.add(rp.getRootHotelId());
                if (rp.getIsDataError()) errorData.add(rp.getRootHotelId());
                if (rp.getMissingData().size() > 0) missingData.add(rp.getRootHotelId());
                keepReports.add(rp);
            }
        }
        report.setExceptions(exceptions);
        report.setErrorData(errorData);
        report.setMissingData(missingData);
        report.setReports(keepReports);
        return report;
    }

    public abstract HotelReport verifyHotel(Long rootHotelId);

    public abstract BaseVerify getSelf();
}
