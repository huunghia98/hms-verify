package vn.tripi.hms.service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import vn.tripi.hms.dao.RoomAvailabilityEntity;
import vn.tripi.hms.dao.RoomAvailabilityMonthlyEntity;
import vn.tripi.hms.dto.HotelReport;
import vn.tripi.hms.dto.PairValue;
import vn.tripi.hms.dto.Report;
import vn.tripi.hms.repo.HotelRepo;
import vn.tripi.hms.repo.RoomAvailMonthlyRepo;
import vn.tripi.hms.repo.RoomAvailRepo;
import vn.tripi.hms.util.DateUtil;
import vn.tripi.hms.util.ExUtil;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class AlmVerify extends BaseVerify {
    @Autowired
    private RoomAvailRepo rangeRepo;
    @Autowired
    private RoomAvailMonthlyRepo monthlyRepo;
    @Autowired
    private AlmVerify self;

    public AlmVerify(ExecutorService executorService, HotelRepo hotelRepo) {
        super(executorService, hotelRepo);
    }

    public HotelReport verifyHotel(Long rootHotelId) {
        HotelReport hotelReport = new HotelReport().setRootHotelId(rootHotelId);
        List<RoomAvailabilityEntity> alms = rangeRepo.findAllByRootHotelId(rootHotelId);
        List<RoomAvailabilityMonthlyEntity> months = monthlyRepo.findAllByRootHotelId(rootHotelId);

        Multimap<Pair<Long, Long>, RoomAvailabilityEntity> almsRoomRateTypes = ArrayListMultimap.create();
        Multimap<Pair<Long, Long>, RoomAvailabilityMonthlyEntity> monthsRoomRateTypes = ArrayListMultimap.create();
        for (var alm : alms) {
            almsRoomRateTypes.put(Pair.of(alm.getRoomId(), alm.getRateTypeId()), alm);
        }
        for (var month : months) {
            monthsRoomRateTypes.put(Pair.of(month.getRoomId(), month.getRateTypeId()), month);
        }
        var mapAlms = almsRoomRateTypes.asMap();
        var mapMonthly = monthsRoomRateTypes.asMap();

        // check monthly contain ranges
        for (var entry : mapAlms.entrySet()) {
            var key = entry.getKey();
            var listRange = entry.getValue();
            boolean isError = checkData(new ArrayList<>(listRange), hotelReport);
            if (isError) {
                hotelReport.setIsDataError(true);
                return hotelReport;
            }
            Map<LocalDate, Long> caches = new HashMap<>();
            for (var alm : listRange) {
                for (LocalDate date = alm.getTimeFrom(); !date.isAfter(alm.getTimeTo()) && date.isBefore(MAX_DATE); date = date.plusDays(1)) {
                    caches.put(date, alm.getNumber());
                }
            }

            var byRoomRateTypeMonthly = monthsRoomRateTypes.get(key);
            if (byRoomRateTypeMonthly == null) {
                hotelReport.getMissingData().add("Missing all key in old ranges: " + key.toString());
                continue;
            }
            for (var monthly : byRoomRateTypeMonthly) {
                int maxDays = LocalDate.of(monthly.getInYear(), monthly.getInMonth(), 1).lengthOfMonth();
                for (int i = 1; i<= maxDays ; i++) {
                    LocalDate date = LocalDate.of(monthly.getInYear(), monthly.getInMonth(), i);
                    if (date.isBefore(MIN_DATE)) continue;
                    Long cacheValue = caches.get(date);
                    long monthlyValue = monthly.getByDay(i);
                    if ((cacheValue == null && monthlyValue != -1000000) || (cacheValue != null && !cacheValue.equals(monthlyValue))) {
                        hotelReport.getMissingData().add(String.format("%s %s-%s-%s old-new: %s - %s", getKey(key), i, monthly.getInMonth(), monthly.getInYear(), cacheValue, monthly.getByDay(i)));
                    }
                }
            }
        }

        // check range contain monthly
        for (var entry : mapMonthly.entrySet()) {
            var key = entry.getKey();
            var listRange = entry.getValue();
            Map<LocalDate, Long> caches = new HashMap<>();
            for (var monthly : listRange) {
                int maxDays = LocalDate.of(monthly.getInYear(), monthly.getInMonth(), 1).lengthOfMonth();
                for (int i = 1; i<= maxDays ; i++) {
                    caches.put(LocalDate.of(monthly.getInYear(), monthly.getInMonth(), i), monthly.getByDay(i));
                }
            }
            var byRoomRateRanges = mapAlms.get(key);
            if (byRoomRateRanges == null) {
                hotelReport.getMissingData().add("Missing all key in monthly: " + key.toString());
                continue;
            }
            for (var range : byRoomRateRanges) {
                for (LocalDate date = range.getTimeFrom(); !date.isAfter(range.getTimeTo())  && date.isBefore(MAX_DATE); date = date.plusDays(1)) {
                    if (date.isBefore(MIN_DATE)) continue;
                    Long cacheValueMonthly = caches.get(date);
                    if (cacheValueMonthly == null || !cacheValueMonthly.equals(range.getNumber())) {
                        hotelReport.getMissingData().add(String.format("%s %s new-old: %s - %s", getKey(key), DateUtil.formatDate(date), cacheValueMonthly, range.getNumber()));
                    }
                }
            }
        }

        return hotelReport;
    }

    @Override
    public BaseVerify getSelf() {
        return self;
    }

    public boolean checkData(List<RoomAvailabilityEntity> alms, HotelReport almReport){
        for (var alm : alms) {
            if (alm.getTimeFrom().isAfter(alm.getTimeTo())) {
                almReport.setDescription("timeFrom > timeTo, id =" + alm.getId());
                return true;
            }
        }
        alms.sort(Comparator.comparing(RoomAvailabilityEntity::getTimeFrom));
        LocalDate lastTo = null;
        for (var range: alms){
            if (lastTo != null && lastTo.compareTo(range.getTimeFrom()) >= 0){
                almReport.setDescription("id " + range.getId());
                return true;
            }
            lastTo = range.getTimeTo();
        }
        return false;
    }

    public String getKey(Pair<Long, Long> key) {
        return "room=" + key.getFirst() + ", rateType" + key.getSecond();
    }
}
