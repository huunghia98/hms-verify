package vn.tripi.hms.dao;

import lombok.SneakyThrows;
import vn.tripi.core.common.jpa.entity.TripiEntity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.time.LocalDate;

@MappedSuperclass
public class BaseAllocationMonthly extends TripiEntity {
    private Long rootHotelId;
    private Long roomId;
    private Long rateTypeId;
    private int inYear;
    private int inMonth;
    private long col01 = -1;
    private long col02 = -1;
    private long col03 = -1;
    private long col04 = -1;
    private long col05 = -1;
    private long col06 = -1;
    private long col07 = -1;
    private long col08 = -1;
    private long col09 = -1;
    private long col10 = -1;
    private long col11 = -1;
    private long col12 = -1;
    private long col13 = -1;
    private long col14 = -1;
    private long col15 = -1;
    private long col16 = -1;
    private long col17 = -1;
    private long col18 = -1;
    private long col19 = -1;
    private long col20 = -1;
    private long col21 = -1;
    private long col22 = -1;
    private long col23 = -1;
    private long col24 = -1;
    private long col25 = -1;
    private long col26 = -1;
    private long col27 = -1;
    private long col28 = -1;
    private long col29 = -1;
    private long col30 = -1;
    private long col31 = -1;

    @Column(name = "root_hotel_id")
    public Long getRootHotelId() {
        return rootHotelId;
    }

    public void setRootHotelId(Long rootHotelId) {
        this.rootHotelId = rootHotelId;
    }

    @Column(name = "room_id")
    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    @Column(name = "rate_type_id")
    public Long getRateTypeId() {
        return rateTypeId;
    }

    public void setRateTypeId(Long rateTypeId) {
        this.rateTypeId = rateTypeId;
    }

    @Column(name = "in_year")
    public int getInYear() {
        return inYear;
    }

    public void setInYear(int inYear) {
        this.inYear = inYear;
    }

    @Column(name = "in_month")
    public int getInMonth() {
        return inMonth;
    }

    public void setInMonth(int inMonth) {
        this.inMonth = inMonth;
    }

    @Column(name = "col01")
    public long getCol01() {
        return col01;
    }

    public void setCol01(long col01) {
        this.col01 = col01;
    }

    @Column(name = "col02")
    public long getCol02() {
        return col02;
    }

    public void setCol02(long col02) {
        this.col02 = col02;
    }

    @Column(name = "col03")
    public long getCol03() {
        return col03;
    }

    public void setCol03(long col03) {
        this.col03 = col03;
    }

    @Column(name = "col04")
    public long getCol04() {
        return col04;
    }

    public void setCol04(long col04) {
        this.col04 = col04;
    }

    @Column(name = "col05")
    public long getCol05() {
        return col05;
    }

    public void setCol05(long col05) {
        this.col05 = col05;
    }

    @Column(name = "col06")
    public long getCol06() {
        return col06;
    }

    public void setCol06(long col06) {
        this.col06 = col06;
    }

    @Column(name = "col07")
    public long getCol07() {
        return col07;
    }

    public void setCol07(long col07) {
        this.col07 = col07;
    }

    @Column(name = "col08")
    public long getCol08() {
        return col08;
    }

    public void setCol08(long col08) {
        this.col08 = col08;
    }

    @Column(name = "col09")
    public long getCol09() {
        return col09;
    }

    public void setCol09(long col09) {
        this.col09 = col09;
    }

    @Column(name = "col10")
    public long getCol10() {
        return col10;
    }

    public void setCol10(long col10) {
        this.col10 = col10;
    }

    @Column(name = "col11")
    public long getCol11() {
        return col11;
    }

    public void setCol11(long col11) {
        this.col11 = col11;
    }

    @Column(name = "col12")
    public long getCol12() {
        return col12;
    }

    public void setCol12(long col12) {
        this.col12 = col12;
    }

    @Column(name = "col13")
    public long getCol13() {
        return col13;
    }

    public void setCol13(long col13) {
        this.col13 = col13;
    }

    @Column(name = "col14")
    public long getCol14() {
        return col14;
    }

    public void setCol14(long col14) {
        this.col14 = col14;
    }

    @Column(name = "col15")
    public long getCol15() {
        return col15;
    }

    public void setCol15(long col15) {
        this.col15 = col15;
    }

    @Column(name = "col16")
    public long getCol16() {
        return col16;
    }

    public void setCol16(long col16) {
        this.col16 = col16;
    }

    @Column(name = "col17")
    public long getCol17() {
        return col17;
    }

    public void setCol17(long col17) {
        this.col17 = col17;
    }

    @Column(name = "col18")
    public long getCol18() {
        return col18;
    }

    public void setCol18(long col18) {
        this.col18 = col18;
    }

    @Column(name = "col19")
    public long getCol19() {
        return col19;
    }

    public void setCol19(long col19) {
        this.col19 = col19;
    }

    @Column(name = "col20")
    public long getCol20() {
        return col20;
    }

    public void setCol20(long col20) {
        this.col20 = col20;
    }

    @Column(name = "col21")
    public long getCol21() {
        return col21;
    }

    public void setCol21(long col21) {
        this.col21 = col21;
    }

    @Column(name = "col22")
    public long getCol22() {
        return col22;
    }

    public void setCol22(long col22) {
        this.col22 = col22;
    }

    @Column(name = "col23")
    public long getCol23() {
        return col23;
    }

    public void setCol23(long col23) {
        this.col23 = col23;
    }

    @Column(name = "col24")
    public long getCol24() {
        return col24;
    }

    public void setCol24(long col24) {
        this.col24 = col24;
    }

    @Column(name = "col25")
    public long getCol25() {
        return col25;
    }

    public void setCol25(long col25) {
        this.col25 = col25;
    }

    @Column(name = "col26")
    public long getCol26() {
        return col26;
    }

    public void setCol26(long col26) {
        this.col26 = col26;
    }

    @Column(name = "col27")
    public long getCol27() {
        return col27;
    }

    public void setCol27(long col27) {
        this.col27 = col27;
    }

    @Column(name = "col28")
    public long getCol28() {
        return col28;
    }

    public void setCol28(long col28) {
        this.col28 = col28;
    }

    @Column(name = "col29")
    public long getCol29() {
        return col29;
    }

    public void setCol29(long col29) {
        this.col29 = col29;
    }

    @Column(name = "col30")
    public long getCol30() {
        return col30;
    }

    public void setCol30(long col30) {
        this.col30 = col30;
    }

    @Column(name = "col31")
    public long getCol31() {
        return col31;
    }

    public void setCol31(long col31) {
        this.col31 = col31;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseAllocationMonthly that = (BaseAllocationMonthly) o;
        return rootHotelId.equals(that.rootHotelId) &&
                roomId.equals(that.roomId) &&
                rateTypeId.equals(that.rateTypeId) &&
                inYear == that.inYear &&
                inMonth == that.inMonth;
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @SneakyThrows
    @Transient
    public long getByDay(long day) {
        return (long) this.getClass().getMethod("getCol" + String.format("%02d", day)).invoke(this);
    }

    @SneakyThrows
    @Transient
    public void setByDay(long day, long value) {
        this.getClass().getMethod("setCol" + String.format("%02d", day), long.class).invoke(this, value);
    }
}
