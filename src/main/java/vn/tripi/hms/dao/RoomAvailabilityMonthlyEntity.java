package vn.tripi.hms.dao;

import vn.tripi.hms.config.TableName;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = TableName.AVAIL_MONTHLY)
public class RoomAvailabilityMonthlyEntity extends BaseAllocationMonthly {
}
