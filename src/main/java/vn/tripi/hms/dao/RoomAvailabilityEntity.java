package vn.tripi.hms.dao;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;
import vn.tripi.core.common.jpa.entity.TripiEntity;
import vn.tripi.hms.config.TableName;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = TableName.AVAIL)
public class RoomAvailabilityEntity extends TripiEntity {
    private Long hotelId;
    private Long rateTypeId;
    private Long roomId;
    private LocalDate timeFrom;
    private LocalDate timeTo;
    private Long number;
    private LocalDateTime updatedTime;
    private Long rootHotelId;

    
    @Column(name = "hotel_id")
    public Long getHotelId() {
        return hotelId;
    }

    
    public RoomAvailabilityEntity setHotelId(Long hotelId) {
        this.hotelId = hotelId;
        return this;
    }

    
    @Column(name = "rate_type_id")
    public Long getRateTypeId() {
        return rateTypeId;
    }

    
    public RoomAvailabilityEntity setRateTypeId(Long rateTypeId) {
        this.rateTypeId = rateTypeId;
        return this;
    }

    
    @Column(name = "room_id")
    public Long getRoomId() {
        return roomId;
    }

    
    public RoomAvailabilityEntity setRoomId(Long roomId) {
        this.roomId = roomId;
        return this;
    }

    
    @Column(name = "time_from")
    public LocalDate getTimeFrom() {
        return timeFrom;
    }

    
    public RoomAvailabilityEntity setTimeFrom(LocalDate timeFrom) {
        this.timeFrom = timeFrom;
        return this;
    }

    
    @Column(name = "time_to")
    public LocalDate getTimeTo() {
        return timeTo;
    }

    
    public RoomAvailabilityEntity setTimeTo(LocalDate timeTo) {
        this.timeTo = timeTo;
        return this;
    }

    
    @Column(name = "number")
    public Long getNumber() {
        return number;
    }

    
    public RoomAvailabilityEntity setNumber(Long number) {
        this.number = number;
        return this;
    }

    
    @UpdateTimestamp
    @Column(name = "updated_time")
    public LocalDateTime getUpdatedTime() {
        return updatedTime;
    }

    public RoomAvailabilityEntity setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime;
        return this;
    }

    @Column(name = "root_hotel_id")
    public Long getRootHotelId() {
        return rootHotelId;
    }

    public RoomAvailabilityEntity setRootHotelId(Long rootHotelId) {
        this.rootHotelId = rootHotelId;
        return this;
    }
}
