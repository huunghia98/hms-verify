package vn.tripi.hms.dao;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;

import vn.tripi.core.common.jpa.entity.TripiEntity;
import vn.tripi.hms.config.TableName;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = TableName.RATE)
public class RoomRateEntity extends TripiEntity {
    private Long hotelId;
    private LocalDate timeFrom;
    private LocalDate timeTo;
    private Long roomId;
    private Long ratePlanId;
    private Long rateTypeId;
    private Long inputPrice;
    private Long rootHotelId;
    private Integer guests;
    private Integer standardGuests;
    private Integer maxGuests;
    private Integer maxChildren;
    private LocalDateTime createdTime;
    private LocalDateTime updatedTime;

    
    @Column(name = "hotel_id")
    public Long getHotelId() {
        return hotelId;
    }

    
    public RoomRateEntity setHotelId(Long hotelId) {
        this.hotelId = hotelId;
        return this;
    }

    
    @Column(name = "time_from")
    public LocalDate getTimeFrom() {
        return timeFrom;
    }

    
    public RoomRateEntity setTimeFrom(LocalDate fromDate) {
        this.timeFrom = fromDate;
        return this;
    }

    
    @Column(name = "time_to")
    public LocalDate getTimeTo() {
        return timeTo;
    }

    
    public RoomRateEntity setTimeTo(LocalDate toDate) {
        this.timeTo = toDate;
        return this;
    }

    
    @Column(name = "input_price")
    public Long getInputPrice() {
        return inputPrice;
    }

    public RoomRateEntity setInputPrice(Long inputPrice) {
        this.inputPrice = inputPrice;
        return this;
    }

    
    @Column(name = "guests")
    public Integer getGuests() {
        return guests;
    }

    
    public RoomRateEntity setGuests(Integer guests) {
        this.guests = guests;
        return this;
    }

    @Column(name = "created_time")
    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public RoomRateEntity setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    @Column(name = "updated_time")
    public LocalDateTime getUpdatedTime() {
        return updatedTime;
    }

    public RoomRateEntity setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime;
        return this;
    }

    @Column(name = "room_id")
    public Long getRoomId() {
        return roomId;
    }

    public RoomRateEntity setRoomId(Long roomId) {
        this.roomId = roomId;
        return this;
    }

    @Column(name = "rate_plan_id")
    public Long getRatePlanId() {
        return ratePlanId;
    }

    public RoomRateEntity setRatePlanId(Long ratePlanId) {
        this.ratePlanId = ratePlanId;
        return this;
    }

    @Column(name = "root_hotel_id")
    
    public Long getRootHotelId() {
        return rootHotelId;
    }

    
    public RoomRateEntity setRootHotelId(Long rootHotelId) {
        this.rootHotelId = rootHotelId;
        return this;
    }

    
    @Column(name = "rate_type_id")
    public Long getRateTypeId() {
        return rateTypeId;
    }

    public RoomRateEntity setRateTypeId(Long rateTypeId) {
        this.rateTypeId = rateTypeId;
        return this;
    }

    
    @Column(name = "standard_guests")
    public Integer getStandardGuests() {
        return standardGuests;
    }

    
    public RoomRateEntity setStandardGuests(Integer standardGuests) {
        this.standardGuests = standardGuests;
        return this;
    }

    
    @Column(name = "max_guests")
    public Integer getMaxGuests() {
        return maxGuests;
    }

    
    public RoomRateEntity setMaxGuests(Integer maxGuests) {
        this.maxGuests = maxGuests;
        return this;
    }

    
    @Column(name = "max_children")
    public Integer getMaxChildren() {
        return maxChildren;
    }

    
    public RoomRateEntity setMaxChildren(Integer maxChildren) {
        this.maxChildren = maxChildren;
        return this;
    }
}
