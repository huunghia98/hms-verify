package vn.tripi.hms.dao;

import vn.tripi.hms.config.TableName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.*;

@Entity
@Table(name = TableName.RATE_MONTHLY)
public class RoomRateMonthlyEntity extends BaseAllocationMonthly {
    private Long ratePlanId;
    private Integer guests;
    private Byte maxGuests;
    private Byte maxChildren;
    private Byte standardGuests;

    @Column(name = "rate_plan_id")
    public Long getRatePlanId() {
        return ratePlanId;
    }

    public void setRatePlanId(Long ratePlanId) {
        this.ratePlanId = ratePlanId;
    }

    @Column(name = "guests")
    public Integer getGuests() {
        return guests;
    }

    public void setGuests(Integer guests) {
        this.guests = guests;
    }

    @Column(name = "max_guests")
    public Byte getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(Byte maxGuests) {
        this.maxGuests = maxGuests;
    }

    @Column(name = "max_children")
    public Byte getMaxChildren() {
        return maxChildren;
    }

    public void setMaxChildren(Byte maxChildren) {
        this.maxChildren = maxChildren;
    }

    @Column(name = "standard_guests")
    public Byte getStandardGuests() {
        return standardGuests;
    }

    public void setStandardGuests(Byte standardGuests) {
        this.standardGuests = standardGuests;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RoomRateMonthlyEntity that = (RoomRateMonthlyEntity) o;
        return ratePlanId.equals(that.ratePlanId) &&
                guests.equals(that.guests);
    }

    public int hashCode() {
        return 31;
    }
}
