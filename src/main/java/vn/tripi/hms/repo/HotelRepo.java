package vn.tripi.hms.repo;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import vn.tripi.core.common.jpa.custom.JPAQuery;
import vn.tripi.core.common.jpa.custom.JPAQueryableRepo;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class HotelRepo extends JPAQueryableRepo<JPAQuery> {
    public HotelRepo(@Qualifier("hmsEntityManager") EntityManager entityManager) {
        super(entityManager);
    }

    public List<Long> getAllRootIds() {
        String query = "select distinct h.root_hotel_id from hotel h where h.root_hotel_id is not null";
        Session session = entityManager.unwrap(Session.class);
        NativeQuery nativeQuery = session.createSQLQuery(query);
        List<Integer> rs = nativeQuery.getResultList();
        return rs.stream().map(Integer::longValue).collect(Collectors.toList());
    }
}
