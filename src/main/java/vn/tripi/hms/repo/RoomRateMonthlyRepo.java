package vn.tripi.hms.repo;

import vn.tripi.core.common.jpa.JPABaseRepo;
import vn.tripi.hms.dao.RoomRateMonthlyEntity;

import java.util.List;

public interface RoomRateMonthlyRepo extends JPABaseRepo<RoomRateMonthlyEntity, Long> {
    List<RoomRateMonthlyEntity> findAllByRootHotelId(Long rootHotelId);
}
