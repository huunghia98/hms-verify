package vn.tripi.hms.repo;

import vn.tripi.core.common.jpa.JPABaseRepo;
import vn.tripi.hms.dao.RoomRateEntity;

import java.util.List;

public interface RoomRateRepo extends JPABaseRepo<RoomRateEntity, Long> {
    List<RoomRateEntity> findAllByRootHotelId(Long rootHotelId);
}
