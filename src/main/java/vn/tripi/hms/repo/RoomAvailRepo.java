package vn.tripi.hms.repo;

import org.springframework.stereotype.Repository;
import vn.tripi.core.common.jpa.JPABaseRepo;
import vn.tripi.hms.dao.RoomAvailabilityEntity;

import java.util.List;

@Repository
public interface RoomAvailRepo extends JPABaseRepo<RoomAvailabilityEntity, Long> {
    List<RoomAvailabilityEntity> findAllByRootHotelId(Long rootHotelId);
}
