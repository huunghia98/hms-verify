package vn.tripi.hms.repo;

import org.springframework.stereotype.Repository;
import vn.tripi.core.common.jpa.JPABaseRepo;
import vn.tripi.hms.dao.RoomAvailabilityEntity;
import vn.tripi.hms.dao.RoomAvailabilityMonthlyEntity;

import java.util.List;

@Repository
public interface RoomAvailMonthlyRepo extends JPABaseRepo<RoomAvailabilityMonthlyEntity, Long> {
    List<RoomAvailabilityMonthlyEntity> findAllByRootHotelId(Long rootHotelId);
}
