# Note Spring project
## Cấu trúc thư mục:
- Build tool gradle
- HMSVerifyApplication : file main dùng để bắt đầu chạy của project
- config: thư mục config các bean, config db, common config
- controller: chứa endpoint các api
- dto: chứa object pojo để tạo response trả về
- service: chứa service logic
- repo: tầng repository lấy dữ liệu từ db tạo các entity
- util: các common utility
- các file resources: 
--  application -> tách ra thành các môi trường local, prd, stage
-- message -> message vi,vn,... 
-- config logback để log,cache config,....
## Cách chạy
Run HMSVerifyApplication
## Curl
Truyền body list hotel id để chạy với hotel_id, không truyền thì duyệt full
- verify allotment: 
```
curl --location --request POST 'localhost:8090/hms-verify/alm' \
--header 'Content-Type: application/json' \
--data-raw '[
    4
]'
```

- verify rate: 
```
curl --location --request POST 'localhost:8090/hms-verify/rate' \
--header 'Content-Type: application/json' \
--data-raw '[
    4
]'
```



